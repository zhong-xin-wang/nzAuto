
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import com.nz.auto.framework.CaseList;
import com.nz.auto.framework.DriverPack;
import com.nz.auto.framework.Log;
import com.nz.auto.framework.OrderContent;
import com.nz.auto.framework.ReadCase;
import com.nz.auto.framework.inter.FuncContent;

/***
 * 通过反射来实现根据类名选择要执行的方法
 * @author NXQ
 *
 */
public class RunTestInvoke {

	public void run(int times) {
		runOrder();
	}

	public void runOrder() {
		List<String[]> list = CaseList.getCaseList();
		ReadCase readCase = new ReadCase();
		for (String[] item : list) {
			DriverPack driverInit = new DriverPack();// 每循环一次就重启一次浏览器
			Map<String, FuncContent> preTo = readCase.ContentConvert(item[0], item[1]);
			Iterator<Entry<String, FuncContent>> iterator=preTo.entrySet().iterator();
			while(iterator.hasNext()) {
				Entry<String, FuncContent> entry=iterator.next();
//				Log.debug(entry.getKey()+":"+entry.getValue());
				OrderContent  oContent=new OrderContent();
				oContent.setClassName(entry.getKey());
				oContent.setArguments(entry.getValue().getArguments());
				oContent.setFunContent(entry.getValue().getPreToCaluse());	
				runOrderInvoke(oContent, driverInit);				
			}
			driverInit.getDriver().quit();

		}
	}

	
	public void runOrderInvoke(OrderContent oContent, DriverPack driverInit)  {
			String className = oContent.getClassName();
			try {
			Class<?> 	class1 = Class.forName(className);
			@SuppressWarnings("rawtypes")
			Class[] parameterTypes= {OrderContent.class, DriverPack.class};
			@SuppressWarnings("rawtypes")
			Constructor constructor=class1.getConstructor(parameterTypes);
			Object[] params= {oContent,driverInit};
			@SuppressWarnings("unused")
			Object object=constructor.newInstance(params);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}

	/**
	 * 测试方法
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		RunTestInvoke test = new RunTestInvoke();
		test.run(1);
	}
}
