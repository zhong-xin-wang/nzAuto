package com.nz.auto.instructions.button;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import com.nz.auto.framework.DriverPack;
import com.nz.auto.framework.Log;

import com.nz.auto.framework.OrderContent;
import com.nz.auto.instructions.Template.InstructionAbstract;

public class InputButton extends InstructionAbstract{
	public int run(OrderContent oContent,DriverPack driverInit) {
		String selector=oContent.getArguments().get(0);
		String Content=oContent.getArguments().get(1);
		try {
			driverInit.getDriver().findElement(By.cssSelector(selector)).sendKeys(Content);
			return 1;
		} catch (NoSuchElementException e) {
			Log.debug("没有找到元素:{}",selector);
			driverInit.getDriver().quit();
			driverInit.getDriver().close();
		}
		return 0;
	}
}
