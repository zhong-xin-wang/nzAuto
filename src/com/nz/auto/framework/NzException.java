package com.nz.auto.framework;

public class NzException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2232082260633035158L;
	String Message;

	public NzException(String string) {
		Log.error(string);
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return Message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		Message = message;
	}
	

}
