package com.nz.auto.core;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.nz.auto.framework.Log;
import com.typesafe.config.*;

/**
 * 指令方言映射获取
 * 打开-openUrl-com.nz.auto.instructions.button.openUrl
 * @author NXQ
 *
 */
public class DialectMap {
	private  Config orderConfig;
	private  Config langConfig;
	private String LangFileName;

	// init
	public DialectMap() {
		Config configBase = ConfigFactory.load("base");
		Config ConfigComplex = ConfigFactory.load("complex");
		Config configCommon = ConfigFactory.parseFile(new File("config/common.conf"));
		this.LangFileName = configCommon.getString("InstructionLanguage") + ".lang";
		langConfig = ConfigFactory.parseFile(new File("i18n/" + LangFileName));
		orderConfig = configBase.withFallback(ConfigComplex);


	}

	/***
	 * 获取实际指令
	 * 
	 * @param ck 中文指令(方言)
	 * @return 返回 指令集中实际指令
	 */
	public String  getInstruction(String ck) {
		
		Set<String> keySet=langConfig.root().keySet();
		Iterator<String> iterator=keySet.iterator();
		String Instruction=null;
		while(iterator.hasNext()) {
			Instruction=iterator.next();
			try {
				String Dialect=langConfig.getString(Instruction);
				if(Dialect.equals(ck)) {
					break;
				}
			} catch (ConfigException e) {
				Log.error("指令集翻译文件:{}中有可能不存在 `{}` 指令",LangFileName,ck );
				throw e;
			}						
		}
		return Instruction;
	}

	/**
	 * 指令集地图
	 */
	public  Map<String , Complex> getInstructionMap() {
		Map<String , Complex> orderMap = new HashMap<String, Complex>();
		Config newConfig=orderConfig.getObject("Root").toConfig();
		Set<String> orderKeySet=newConfig.root().keySet();
		Iterator<String> iterator=orderKeySet.iterator();
		while(iterator.hasNext()) {
			String orderKey=iterator.next();
			Config newConfigDetail=newConfig.getConfig(orderKey);
			String DialectOrderName=langConfig.getString(orderKey);
			int argsNum=(int)newConfigDetail.getValue("arg").unwrapped();
			String classMappingName=newConfigDetail.getValue("class").unwrapped().toString();
			int strucLevel=(int)newConfigDetail.getValue("struc").unwrapped();
			Complex complex=new Complex();
			complex.setOrderName(orderKey);
			complex.setDialectOrderName(DialectOrderName);
			complex.setArgsNum(argsNum);
			complex.setClassMappingName(classMappingName);
			complex.setStrucLevel(strucLevel);
			orderMap.put(orderKey, complex);			
		}
		return orderMap;
	}

	// 测试方法-获取指令复杂度
	/***
	 * 获取指令复杂度
	 * @param Instruction: 方言指令
	 * @return InstructionComplexity 指令复杂度
	 */
	public int getInstructionComplexity(String ck) {
		String Instruction=getInstruction(ck);
		return getInstructionMap().get(Instruction).getStrucLevel();
	}



	/***
	 * 获取指令复杂度
	 * @param Instruction: 方言指令
	 * @return int 指令的参数个数
	 */
	public int getParam(String ck) {
		String Instruction=getInstruction(ck);
		return getInstructionMap().get(Instruction).getArgsNum();
	}
	
	//测试方法-获取执行指令的类
	public String getClassName(String ck) {
		String Instruction=getInstruction(ck);
		return getInstructionMap().get(Instruction).getClassMappingName();
	}
	
	
	// 测试方法: 测试用例获取指令集,输入指令获取实际指令和指令类.
	public static void main(String[] args) {
		DialectMap read = new DialectMap();
		Log.debug(read.getInstructionMap());
		Log.debug(read.getInstruction("切换新窗口"));
		Log.debug("实际类名"+read.getClassName("切换新窗口"));
		Log.debug("参数个数"+read.getParam("切换新窗口"));
		Log.debug(read.getInstructionComplexity("打开"));

	}

	/**
	 * 获取绝对路径
	 * 
	 * @param FileName
	 * @return
	 */
	@Deprecated
	public static String getAbsoutePath(String FileName) {
		return Thread.currentThread().getContextClassLoader().getResource("").toString().substring(6) + FileName;
	}

}

