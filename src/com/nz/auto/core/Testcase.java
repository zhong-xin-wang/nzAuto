/**  
 * All rights Reserved, Designed By www.pcyo.cn
 * @Title:  Testcase.java   
 * @Package com.nz.auto.core   
 * @Description:    TODO 暂时未用到的类
 * @author: AmoiBrush     
 * @date:   2018年7月6日 下午4:45:25   
 * @version V1.0 
 * @Copyright: 2018 www.pcyo.cn Inc. All rights reserved. 
 * 
 */
package com.nz.auto.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;


/**
 * @author NXQ
 * @param <E>
 *
 */
public class Testcase<E> {

//	private List<E> InputOrders;//左指令集
//	private List<E> InputContent;//右参数内容(可单行,可多行,针对循环判断执行.)
//	
//	

	private String CasePath;

	/***
	 * @Title: getCasePath <BR>
	 * @Description: 获取用例路径
	 * @Field String <BR>
	 * @return the casePath
	 */
	public String getCasePath() {
		return CasePath;
	}

	/***
	 * 
	 * @Title: setCasePath <BR>
	 * @Description: 设置用例路径
	 * @return: String <BR>
	 * @param casePath the casePath to set
	 */
	public void setCasePath(String casePath) {
		CasePath = casePath;
	}

	/**
	 * @Title: main @Description: 测试方法 @param: @param args @return: void @throws
	 */
	public static void main(String[] args) {
		

	}

	/***
	 *
	 * @Title: readCaseFile @Description: 读取用例文件 @param: @return @return:
	 * String @throws
	 */
	public void readCaseFile(String CasePath) {
		String encoding = "GBK";
		File file = new File(CasePath);
		if (file.isFile() && file.exists()) {// 判断文件是否存在,且必须是文件
			try {
				InputStreamReader reader = new InputStreamReader(new FileInputStream(file), encoding);
				BufferedReader bufferedReader = new BufferedReader(reader);
				
				bufferedReader.close();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}

/***
 * 顺序结构
 * 
 * @author NXQ
 *
 */
class SequenceSture {

}

/***
 * 循环结构
 * 
 * @author NXQ
 *
 */
class LoopStructure {

}

/***
 * 选择结构
 * 
 * @author NXQ
 *
 */
class SelecttionStructure {

}
