package com.nz.test.auto.inter;



import com.nz.test.auto.core.DriverPack;

/***
 * 回调接口.
 * @author NXQ
 *
 */
public interface CallBack {
	public void returnResult(String result);
	public OrderContent getContent();
	public DriverPack getDriver();
}
