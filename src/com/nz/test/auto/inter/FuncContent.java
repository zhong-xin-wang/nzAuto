package com.nz.test.auto.inter;

import java.util.List;

/**
 * 参数表,待执行子句
 */
public class FuncContent{
	private String className;
	private List<String> arguments;
	private FuncContent funcContent;
	private List<String> preToCaluse;
	/**  
	 * @Title:  getArguments <BR>  
	 * @Description: please write your description <BR>  
	 * @Field String[] <BR>  
	 * @return the arguments
	 */
	public List<String> getArguments() {
		return arguments;
	}
	/**  
	 * @Title:  setArguments <BR>  
	 * @Description: please write your description <BR>  
	 * @return: String[] <BR>  
	 * @param arguments the arguments to set
	 */
	public void setArguments(List<String> arguments) {
		this.arguments = arguments;
	}
	/**  
	 * @Title:  getPreToCaluse <BR>  
	 * @Description: please write your description <BR>  
	 * @Field String[] <BR>  
	 * @return the preToCaluse
	 */
	public List<String> getPreToCaluse() {
		return preToCaluse;
	}
	/**  
	 * @Title:  setPreToCaluse <BR>  
	 * @Description: please write your description <BR>  
	 * @return: String[] <BR>  
	 * @param preToCaluse the preToCaluse to set
	 */
	public void setPreToCaluse(List<String> preToCaluse) {
		this.preToCaluse = preToCaluse;
	}
	/**
	 * @return the funcContent
	 */
	public FuncContent getFuncContent() {
		return funcContent;
	}
	/**
	 * @param funcContent the funcContent to set
	 */
	public void setFuncContent(FuncContent funcContent) {
		this.funcContent = funcContent;
	}
	/**
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}
	/**
	 * @param className the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}
}
