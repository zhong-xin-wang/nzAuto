package com.nz.test.auto.instruction;


import com.nz.test.auto.core.InstructionStruc;
import com.nz.test.auto.inter.CallBack;
import com.nz.test.auto.inter.Runner;

public class OpenUrl  extends InstructionStruc implements Runner   {




	@Override
	public void processDetail(CallBack callBack) {
		try {
			this.setContent(callBack.getContent());
			this.setDriverPack(callBack.getDriver());			
			String url=this.getContent().getArguments().get(0);
			this.getDriverPack().getDriver().get(url);
			Thread.sleep(1000);
			callBack.returnResult("打开url:"+url);
		} catch (Exception e) {
		}
	}
}
